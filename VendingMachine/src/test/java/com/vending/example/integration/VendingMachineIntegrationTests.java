package com.vending.example.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vending.example.model.Coin;
import com.vending.example.model.Product;
import com.vending.example.model.dto.BuyDto;
import com.vending.example.payload.enums.ProductType;
import com.vending.example.model.dto.CoinDto;
import com.vending.example.payload.request.BuyRequest;
import com.vending.example.payload.request.CoinRequest;
import com.vending.example.model.dto.ProductDto;
import com.vending.example.payload.request.ProductRequest;
import com.vending.example.repository.CoinRepository;
import com.vending.example.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class VendingMachineIntegrationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CoinRepository coinRepository;

	@Test
	public void getAllProducts_shouldSucceed() throws Exception {

		populateProduct();

		this.mockMvc.perform( MockMvcRequestBuilders
				.get("/api/inventory/products") )
				.andExpect(status().isOk());

		Collection<Product> productCollection = productRepository.findAll();
		assertNotNull(productCollection);

		deleteEntries();
	}

	@Test
	public void createNewProduct_shouldSucceed() throws Exception {

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/inventory/product/create" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(populateProductRequestDto())))
				.andExpect(status().isOk());

		Product product = productRepository.findByType( ProductType.CHOCOLATE.name() );
		assertEquals(product.getAvailable(), true);

		deleteEntries();
	}

	@Test
	public void createNewProduct_ThrownError() throws Exception {

		Product product = new Product();
		product.setType(ProductType.CANDY.name());
		product.setQuantity(10);
		product.setPrice(BigDecimal.valueOf(0.8));
		product.setAvailable(true);
		this.productRepository.save(product);

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/inventory/product/create" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(populateProductRequestDto())))
				.andExpect(status().is5xxServerError());

		deleteEntries();

	}

	@Test
	public void getProductByType_shouldSucceed() throws Exception {
		populateProduct();

		this.mockMvc.perform( MockMvcRequestBuilders
				.get("/api/inventory/product/coffee") )
				.andExpect(status().isOk());

		deleteEntries();
	}

	@Test
	public void getProductByType_shouldThrownError() throws Exception {
		populateProduct();

		this.mockMvc.perform( MockMvcRequestBuilders
				.get("/api/inventory/product/milk") )
				.andExpect(status().is4xxClientError());

		deleteEntries();
	}

	@Test
	public void removeProduct_shouldThrownError() throws Exception {
		populateProduct();

		this.mockMvc.perform( MockMvcRequestBuilders
				.delete("/api/inventory/product/chocolate/remove" ) )
				.andExpect(status().is4xxClientError() );

		deleteEntries();

	}

	@Test
	public void insertCoins_shouldSucceed() throws Exception {

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/vending/insert" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString( populateCoinRequest() )))
				.andExpect(status().isOk());

		List<Coin> coin = this.coinRepository.findAll();
		assertNotNull(coin);

	}

	@Test
	public void insertCoins_shouldThrownException() throws Exception {

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/vending/insert" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString( populateWrongCoinRequest() )))
				.andExpect(status().is4xxClientError());

	}

	@Test
	public void resetCoins_shouldSucceed() throws Exception {

		Coin coin = new Coin();
		coin.setTotalCoins( BigDecimal.valueOf(3.4) );
		this.coinRepository.save(coin);

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/vending/reset" ) )
				.andExpect(status().isOk());

		List<Coin> coinList = this.coinRepository.findAll();
		assertTrue(coinList.isEmpty());

	}

	@Test
	public void buyProduct_shouldSucceed() throws Exception {

		this.mockMvc.perform( MockMvcRequestBuilders
				.post("/api/vending/product/buy" )
				.contentType("application/json")
				.content(objectMapper.writeValueAsString( populateBuyProductRequest() )) )
				.andExpect(status().isOk());

		List<Coin> coinList = this.coinRepository.findAll();
		assertTrue(coinList.isEmpty());

		deleteEntries();

	}

	private BuyRequest populateBuyProductRequest(){

		Coin coin = new Coin();
		coin.setTotalCoins( BigDecimal.valueOf(6.4) );
		this.coinRepository.save(coin);

		Product product1 = new Product();
		product1.setQuantity(7);
		product1.setAvailable(true);
		product1.setPrice(BigDecimal.valueOf(0.5));
		product1.setType(ProductType.COFFEE.name());

		this.productRepository.save(product1);

		Product product2 = new Product();
		product2.setQuantity(5);
		product2.setAvailable(true);
		product2.setPrice(BigDecimal.valueOf(1.5));
		product2.setType(ProductType.CHIPS.name());

		this.productRepository.save(product2);

		BuyDto dto1 = new BuyDto(ProductType.COFFEE.name(), 2);
		BuyDto dto2 = new BuyDto(ProductType.JUICE.name(), 1);
		BuyDto dto3 = new BuyDto(ProductType.CHOCOLATE.name(), 2);
		BuyDto dto4 = new BuyDto(ProductType.CHIPS.name(), 2);

		List<BuyDto> products = new ArrayList<>();
		products.add(dto1);
		products.add(dto2);
		products.add(dto3);
		products.add(dto4);

		return new BuyRequest(products);

	}

	private ProductRequest populateProductRequestDto(){

		ProductDto dto1 = new ProductDto(new BigDecimal(10), ProductType.CHOCOLATE.name(), 2, true);
		ProductDto dto2 = new ProductDto(new BigDecimal(10), ProductType.COKE.name(), 5, true);
		ProductDto dto3 = new ProductDto(new BigDecimal(10), ProductType.CANDY.name(), 10, true);

		List<ProductDto> productDtos = new ArrayList<>();
		productDtos.add(dto1);
		productDtos.add(dto2);
		productDtos.add(dto3);

		return new ProductRequest(productDtos);
	}

	private CoinRequest populateCoinRequest(){
		CoinDto coinDto1 = new CoinDto(0.2);
		CoinDto coinDto2 = new CoinDto(0.5);
		CoinDto coinDto3 = new CoinDto(1);

		List<CoinDto> coinDtoList = new ArrayList<>();
		coinDtoList.add(coinDto1);
		coinDtoList.add(coinDto2);
		coinDtoList.add(coinDto3);

		return new CoinRequest(coinDtoList);
	}

	private CoinRequest populateWrongCoinRequest(){
		CoinDto coinDto1 = new CoinDto(0.3);
		CoinDto coinDto2 = new CoinDto(0.7);
		CoinDto coinDto3 = new CoinDto(1);

		List<CoinDto> coinDtoList = new ArrayList<>();
		coinDtoList.add(coinDto1);
		coinDtoList.add(coinDto2);
		coinDtoList.add(coinDto3);

		return new CoinRequest(coinDtoList);
	}

	private void populateProduct(){
		Product product = new Product();
		product.setAvailable(true);
		product.setPrice(new BigDecimal(1.5));
		product.setQuantity(4);
		product.setType(ProductType.COFFEE.name());

		this.productRepository.save(product);
	}

	private void deleteEntries(){
		this.productRepository.deleteAll();
	}

}

package com.vending.example.model.dto;

public class CoinDto {

    private double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public CoinDto() {
    }

    public CoinDto(double value) {
        this.value = value;
    }
}

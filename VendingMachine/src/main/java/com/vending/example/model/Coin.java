package com.vending.example.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table( name = "coins")
public class Coin {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long Id;

    @Column( name = "total_coins")
    private BigDecimal totalCoins;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public BigDecimal getTotalCoins() {
        return totalCoins;
    }

    public void setTotalCoins(BigDecimal totalCoins) {
        this.totalCoins = totalCoins;
    }
}

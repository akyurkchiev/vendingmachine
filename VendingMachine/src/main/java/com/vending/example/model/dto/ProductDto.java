package com.vending.example.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class ProductDto {

    @JsonProperty("price")
    private BigDecimal price;

    @JsonProperty("type")
    private String type;

    @JsonProperty("quantity")
    private int quantity;

    @JsonProperty("available")
    private Boolean available;

    public ProductDto(){}

    public ProductDto(BigDecimal price, String type, int quantity, Boolean available) {
        this.price = price;
        this.type = type;
        this.quantity = quantity;
        this.available = available;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}

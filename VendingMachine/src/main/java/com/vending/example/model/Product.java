package com.vending.example.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table( name = "products")
public class Product {


    public Product() {
    }

    public Product(BigDecimal price, String type, int quantity, Boolean available) {
        this.price = price;
        this.type = type;
        this.quantity = quantity;
        this.available = available;
    }

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long Id;

    @Column( name = "price")
    private BigDecimal price;

    @Column( name = "type", unique = true)
    private String type;

    @Column( name = "quantity")
    private int quantity;

    @Column( name = "available")
    private Boolean available = false;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

}

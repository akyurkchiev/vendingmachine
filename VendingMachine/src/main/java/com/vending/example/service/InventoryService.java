package com.vending.example.service;

import com.vending.example.exceptions.ProductNotFoundException;
import com.vending.example.model.Product;
import com.vending.example.model.dto.ProductDto;
import com.vending.example.payload.request.ProductRequest;
import com.vending.example.repository.ProductRepository;
import com.vending.example.util.Helper;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class InventoryService {

    @Autowired
    private ProductRepository productRepository;

    public Collection<Product> getAll(){
        return productRepository.findAll();
    }

    public ProductDto findByType(String type ) {

        Product product = productRepository.findByType( type );
        if ( product == null  ){
            throw new ProductNotFoundException( type );
        }

        ModelMapper mapper = new ModelMapper();
        ProductDto existingProductDto = new ProductDto();

        mapper.map(product, existingProductDto);

        return existingProductDto;
    }

    public void saveOrUpdate(ProductRequest dto) {

        for (ProductDto productDto : dto.getProducts()){
            if ( productRepository.findByType(productDto.getType() ) != null){
                Product currentProduct = productRepository.findByType( productDto.getType() );
                Helper.checkAndUpdateQuantity( productDto.getQuantity(),currentProduct.getQuantity() );
                currentProduct.setQuantity(Helper.checkAndUpdateQuantity( productDto.getQuantity(),currentProduct.getQuantity() ) );
                currentProduct.setPrice(productDto.getPrice());
                currentProduct.setAvailable(productDto.getAvailable());
                productRepository.save(currentProduct);
            } else{
                Product newProduct = Helper.convertDtoToEntity(productDto);
                this.productRepository.save(newProduct);
            }
        }
    }

    public void delete(String type) {

        Product product = productRepository.findByType(type);

        if (ObjectUtils.isEmpty(product)){
            throw new ProductNotFoundException(type);
        }

        productRepository.delete(product);
    }
}

package com.vending.example.service;

import com.vending.example.exceptions.CoinCoreException;
import com.vending.example.exceptions.CoinNotAcceptableException;
import com.vending.example.exceptions.ProductCoreException;
import com.vending.example.model.Coin;
import com.vending.example.model.Product;
import com.vending.example.model.dto.BuyDto;
import com.vending.example.model.dto.ProductDto;
import com.vending.example.payload.request.BuyRequest;
import com.vending.example.payload.response.BuyResponse;
import com.vending.example.model.dto.CoinDto;
import com.vending.example.repository.CoinRepository;
import com.vending.example.repository.ProductRepository;
import com.vending.example.util.Helper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class VendingService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CoinRepository coinRepository;

    public void insertCoins( List<CoinDto> coinDtos) {

        if ( !Helper.isAllowedCoin(coinDtos).isEmpty()) {

            for (CoinDto coinDto : Helper.isAllowedCoin(coinDtos) ){
                throw new CoinNotAcceptableException(coinDto.getValue());
            }
        }
        sumTotalValueOfEveryCoin( coinDtos );
    }

    public double returnCoin() {

        List<Coin> coinList = this.coinRepository.findAll();

        BigDecimal sumCoins = BigDecimal.ZERO;
        for (Coin coin: coinList){
            sumCoins = sumCoins.add(coin.getTotalCoins());
            this.coinRepository.delete(coin);
        }


        return sumCoins.doubleValue();
    }

    public BuyResponse buyProduct(BuyRequest request ){

        List<Product> wantedProducts = new ArrayList<>();
        for (BuyDto dto : request.getProductTypes()){
            Product existingProduct = productRepository.findByType( dto.getType() );
            if ( existingProduct != null ){
                if(existingProduct.getQuantity() > dto.getQuantity() && existingProduct.getAvailable().equals(true) ){
                    int newQuantity = existingProduct.getQuantity() - dto.getQuantity();
                    existingProduct.setQuantity(newQuantity);
                    this.productRepository.save(existingProduct);
                } else {
                    throw new ProductCoreException("Not enough quantity from this product " + existingProduct.getType());
                }
                wantedProducts.add(existingProduct);
            }
        }

        BigDecimal change = enoughMoneyToBuy( wantedProducts );
        List<String> productTypes = new ArrayList<>();
        for (Product p: wantedProducts){
            productTypes.add(p.getType());
        }

        return new BuyResponse( productTypes, change);
    }

    private void sumTotalValueOfEveryCoin(List<CoinDto> coinDtos) {

        double sum = 0;
        for (CoinDto dto: coinDtos){
            sum += dto.getValue();
        }
        Coin coin = new Coin();
        coin.setTotalCoins( BigDecimal.valueOf(sum) );
        this.coinRepository.save(coin);
    }

    private BigDecimal enoughMoneyToBuy( List<Product> wantedProducts ){

        BigDecimal currentMoney = checkForMoney();
        BigDecimal productsValue = BigDecimal.ZERO;
        for (Product p : wantedProducts){
            productsValue = productsValue.add(p.getPrice());
        }

        BigDecimal result = currentMoney.subtract(productsValue);
        if (result.compareTo(BigDecimal.ZERO) < 0){
            throw new CoinCoreException("You don't have enough money to buy this");
        }

        return currentMoney.subtract(productsValue);

    }

    private BigDecimal checkForMoney(){
        List<Coin> coinList = this.coinRepository.findAll();
        BigDecimal sumCoins = BigDecimal.ZERO;
        if (coinList.isEmpty()){
            throw new CoinCoreException("You have no money inserted");
        }
        for (Coin coin: coinList){
            sumCoins = sumCoins.add(coin.getTotalCoins());
            this.coinRepository.delete(coin);
        }

        return sumCoins;
    }

}

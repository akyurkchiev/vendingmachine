package com.vending.example.controller;

import com.vending.example.payload.request.BuyRequest;
import com.vending.example.payload.request.CoinRequest;
import com.vending.example.payload.response.BuyResponse;
import com.vending.example.service.VendingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( "/api/vending" )
public class VendingController {

    Logger logger = LoggerFactory.getLogger(VendingController.class);

    @Autowired
    private VendingService vendingService;

    @PostMapping( "/insert" )
    public void insertCoin( @RequestBody CoinRequest request ) {

        logger.info("Insert coins");
        vendingService.insertCoins( request.getCoinDtos() );

    }

    @PostMapping( "/reset")
    public ResponseEntity<Double> returnCoin() {

        logger.info("Return coins");

        return ResponseEntity.ok( vendingService.returnCoin() );
    }

    @PostMapping( "/product/buy" )
    @ResponseBody
    public ResponseEntity<BuyResponse> buy(@RequestBody BuyRequest request ){

        logger.info( "Buy new product" );


        BuyResponse response = vendingService.buyProduct( request );

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}

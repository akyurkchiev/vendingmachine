package com.vending.example.controller;

import com.vending.example.exceptions.ProductNotFoundException;
import com.vending.example.model.Product;
import com.vending.example.model.dto.ProductDto;
import com.vending.example.payload.request.ProductRequest;
import com.vending.example.service.InventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping( "/api/inventory" )
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    private Logger logger = LoggerFactory.getLogger(InventoryController.class);

    @GetMapping( "/products")
    public ResponseEntity<Collection<Product>> findAll() {

        return ResponseEntity.ok( inventoryService.getAll() );
    }

    @GetMapping( "/product/{type}")
    public ResponseEntity<ProductDto> get(@PathVariable String type ) {

        return ResponseEntity.ok(inventoryService.findByType( type ));
    }

    @PostMapping( value = "/product/create", consumes = {MediaType.APPLICATION_JSON_VALUE} )
    public void create( @RequestBody ProductRequest requestDto )
            throws ProductNotFoundException {

        logger.info("Create new product from ");
        inventoryService.saveOrUpdate(requestDto);

    }

    @DeleteMapping("/product/{type}/remove")
    public ResponseEntity<String> remove(@PathVariable("type") String type)
            throws ProductNotFoundException {

        inventoryService.delete(type);

        return new ResponseEntity<>(type, HttpStatus.OK);
    }

}

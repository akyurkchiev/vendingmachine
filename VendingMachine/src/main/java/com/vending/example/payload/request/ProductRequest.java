package com.vending.example.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vending.example.model.dto.ProductDto;

import java.io.Serializable;
import java.util.List;

public class ProductRequest implements Serializable {

    @JsonProperty("products")
    List<ProductDto> products;

    public ProductRequest() {
    }

    public ProductRequest(List<ProductDto> products) {
        this.products = products;
    }

    public List<ProductDto> getProducts() {
        return products;
    }
}

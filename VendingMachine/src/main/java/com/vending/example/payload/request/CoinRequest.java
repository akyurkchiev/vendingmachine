package com.vending.example.payload.request;

import com.vending.example.model.dto.CoinDto;

import java.util.List;

public class CoinRequest {

    private List<CoinDto> coinDtos;

    public List<CoinDto> getCoinDtos() {
        return coinDtos;
    }

    public CoinRequest() {
    }

    public CoinRequest(List<CoinDto> coinDtos) {
        this.coinDtos = coinDtos;
    }

    public void setCoinDtos(List<CoinDto> coinDtos) {
        this.coinDtos = coinDtos;
    }
}

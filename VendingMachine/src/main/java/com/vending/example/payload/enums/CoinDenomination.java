package com.vending.example.payload.enums;

import java.util.EnumMap;

public enum CoinDenomination {

    TEN_STOTINKI(0.1),
    TWENTY_STOTINKI(0.2),
    FIFTY_STOTINKI(0.5),
    ONE_LEV(1),
    TWO_LEVA(2);

    private double value;

    CoinDenomination(double value) {
        this.value = value;
    }

    public double getValue() {
        return this.value;
    }

    public static boolean checkValue( double value ) {

        EnumMap<CoinDenomination, Double> enumMap = new EnumMap<>(CoinDenomination.class);

        for (CoinDenomination coin : values()){
            enumMap.put( coin, coin.getValue() );
        }

        return enumMap.containsValue(value) ? true : false;

    }

}

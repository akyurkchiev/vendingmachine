package com.vending.example.payload.response;

import java.math.BigDecimal;
import java.util.List;

public class BuyResponse {

    private List<String> productTypes;

    private BigDecimal change;

    public BuyResponse(List<String> productTypes, BigDecimal change) {
        this.productTypes = productTypes;
        this.change = change;
    }

    public List<String> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<String> productTypes) {
        this.productTypes = productTypes;
    }

    public BigDecimal getChange() {
        return change;
    }

    public void setChange(BigDecimal change) {
        this.change = change;
    }
}

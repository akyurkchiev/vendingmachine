package com.vending.example.payload.request;

import com.vending.example.model.dto.BuyDto;

import java.util.List;

public class BuyRequest {

    List<BuyDto> productTypes;

    public BuyRequest() {

    }

    public BuyRequest(List<BuyDto> productTypes) {
        this.productTypes = productTypes;
    }

    public List<BuyDto> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<BuyDto> productTypes) {
        this.productTypes = productTypes;
    }
}

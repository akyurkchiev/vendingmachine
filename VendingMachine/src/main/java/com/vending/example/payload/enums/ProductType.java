package com.vending.example.payload.enums;

public enum ProductType {

    COKE,
    COFFEE,
    CHOCOLATE,
    WAFFLE,
    CHIPS,
    ICE_CREAM,
    CANDY,
    JUICE
}

package com.vending.example.repository;

import com.vending.example.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query( "Select p From Product p Where p.type = ?1" )
    Product findByType(String type );

}

package com.vending.example.util;

import com.vending.example.exceptions.ProductCoreException;
import com.vending.example.model.Product;
import com.vending.example.model.dto.CoinDto;
import com.vending.example.model.dto.ProductDto;
import com.vending.example.payload.enums.CoinDenomination;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Helper {

    public static List<CoinDto> isAllowedCoin(List<CoinDto> coinDtos){
        List<CoinDto> list = new ArrayList<>();
        for (CoinDto coinDto : coinDtos) {
           if ( !CoinDenomination.checkValue(coinDto.getValue()) ){
               list.add(coinDto);
           }
        }
        return list;
    }

    public static int checkAndUpdateQuantity( int quantityFromRequest, int currentQuantity){

        if ( currentQuantity == 10 || quantityFromRequest + currentQuantity >= 10 ){
            throw new ProductCoreException("Cannot be stored more than 10 products from same type");
        }

        return quantityFromRequest + currentQuantity;
    }

    public static Product convertDtoToEntity(ProductDto requestDto){

        Product product = new Product();
        product.setType(requestDto.getType());
        product.setQuantity(requestDto.getQuantity());
        product.setPrice(requestDto.getPrice());
        product.setAvailable(requestDto.getAvailable());

        return product;

    }

    public List<ProductDto> convertEntityToDto( List<Product> products){

        List<ProductDto> productDtos = new ArrayList<>();

        if ( !products.isEmpty() ){

            productDtos = products.stream().map( p -> {
                ProductDto productDto = new ProductDto();
                productDto.setType(p.getType());
                productDto.setQuantity(p.getQuantity());
                productDto.setPrice(p.getPrice());
                productDto.setAvailable(p.getAvailable());

                return productDto;

            }).collect(Collectors.toList());
        }
        return productDtos;
    }

}

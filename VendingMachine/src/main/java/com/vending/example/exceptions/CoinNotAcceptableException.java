package com.vending.example.exceptions;

public class CoinNotAcceptableException extends RuntimeException {

    public CoinNotAcceptableException(double value) {
        super("Coin with this value is not accepted : " + value);
    }
}

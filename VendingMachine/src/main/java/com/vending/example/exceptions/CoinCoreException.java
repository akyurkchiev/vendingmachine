package com.vending.example.exceptions;

public class CoinCoreException extends RuntimeException {

    public CoinCoreException(String msg) {
        super("Problem with the coins : " + msg);
    }
}

package com.vending.example.exceptions;

public class ProductCoreException extends RuntimeException {

    public ProductCoreException(String msg) {
        super("Problem with the product : " + msg);
    }
}

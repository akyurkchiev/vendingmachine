package com.vending.example.exceptions;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(String type) {
        super("Product with this type not found : " + type);
    }
}

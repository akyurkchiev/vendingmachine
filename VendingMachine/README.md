# Description

Project is about vending machine and to manage and buy different products with coins. Main functionalities are to be able to create, update, delete new products.

##Database setup

For the purposes of this project has been used relational database MariaDB. It runs as docker service, but it could be run as local mysql instance.
For docker use the following command below:

```bash
docker run --name mariadb -e MYSQL_ROOT_PASSWORD=atanas90 -e MYSQL_DATABASE=dbtest -p 3306:3306 -d mariadb
```

In order to have access to database, modify the application.properties file in the following rows:

```python
spring.datasource.url=jdbc:mariadb://localhost:3306/dbtest
spring.datasource.username=root
spring.datasource.password=atanas90
```

## Build and Installation

The project is based ot gradle and java and for the reason gradle and jdk with Java8 or above should be available.

In order to build the project, use the following command in project directory
```bash
./gradlew clean build
```

In order to run the application, use the following command
```bash
./gradlew bootRun
```

The application will be available on http://localhost:4522

## Public Documentation

Public documentation is integrated with swagger. After the application is up and running, it will be visible in the browser. Just visit http://localhost:8080/swagger-ui.html#/